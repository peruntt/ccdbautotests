package services;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.response.Response;

public interface CcdbAccountClientIf {

    Response getAccount(String systemName, String id, String host);

    Response getAccount(String ucid, String host);

    Response searchAccount(String requestBody, String host);

    Response createOrUpdateAccount(JsonNode requestBody, String host);

    Response deleteAccount(String systemName, String id, String host);

    Response deleteAccount(String ucid, String host);
}
