package services;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.response.Response;

public interface CcdbContactClientIf {
    Response getContact(String systemName, String id, String host);

    Response getContact(String uconId, String host);

    Response searchContact(String requestBody, String host);

    Response createOrUpdateContact(JsonNode requestBody, String host);

    Response deleteContact(String systemName, String id, String host);

    Response deleteContact(String ucid, String host);
}
