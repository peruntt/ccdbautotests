package services;

import io.restassured.response.Response;

public interface TestDataProviderIf {

    void prepareTestData();

    void insertTestData(final String apiHost);

    void prepareTestData(String externalSysName);

    Response insertTestData(String externalSysName, String apiHostUrl);

    void removeTestData();
}
