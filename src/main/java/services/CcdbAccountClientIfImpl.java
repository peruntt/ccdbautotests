package services;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.thucydides.core.annotations.Step;
import org.springframework.stereotype.Service;
import utils.PropertyHelper;

import java.util.Properties;

import static net.serenitybdd.rest.SerenityRest.rest;

@Service
@NoArgsConstructor
@Data
public class CcdbAccountClientIfImpl implements CcdbAccountClientIf {

    private Properties ccdbProperties = PropertyHelper.loadProperty("src/main/resources/endpoint.properties");
    private final String accountUri = ccdbProperties.getProperty("accountURI");

    @Override
    @Step
    public Response getAccount(final String systemName, final String id, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .queryParam("SystemName", systemName)
                .queryParam("Id", id)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .get(host + accountUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    @Step
    public Response getAccount(final String ucid, final String host) {
        return rest().given()
                .pathParam("ucid", ucid)
                .accept(ContentType.JSON)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .get(host + accountUri + "/{ucid}")
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    @Step
    public Response searchAccount(final String requestBody, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .relaxedHTTPSValidation()
                .body(requestBody)
                .log()
                .all()
                .when()
                .post(host + accountUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    @Step
    public Response createOrUpdateAccount(final JsonNode requestBody, final String host) {
        return rest().given()
                .contentType("application/json")
                .accept(ContentType.JSON)
                .relaxedHTTPSValidation()
                .body(requestBody)
                .log()
                .all()
                .when()
                .put(host + accountUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    @Step
    public Response deleteAccount(final String systemName, final String id, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .queryParam("SystemName", systemName)
                .queryParam("Id", id)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .delete(host + accountUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    @Step
    public Response deleteAccount(final String ucid, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .pathParam("ucid", ucid)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .get(host + accountUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }
}
