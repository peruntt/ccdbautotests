package services;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import utils.PropertyHelper;

import java.util.Properties;

import static net.serenitybdd.rest.SerenityRest.rest;

public class CcdbContactClientIfImpl implements CcdbContactClientIf {

    private Properties ccdbProperties = PropertyHelper.loadProperty("src/main/resources/endpoint.properties");
    private final String contactUri = ccdbProperties.getProperty("contactURI");

    @Override
    public Response getContact(final String systemName, final String id, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .queryParam("SystemName", systemName)
                .queryParam("Id", id)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .get(host + contactUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    public Response getContact(final String uconId, final String host) {
        return rest().given()
                .pathParam("uconId", uconId)
                .accept(ContentType.JSON)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .get(host + contactUri + "/{uconId}")
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    public Response searchContact(final String requestBody, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .relaxedHTTPSValidation()
                .body(requestBody)
                .log()
                .all()
                .when()
                .post(host + contactUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    public Response createOrUpdateContact(final JsonNode requestBody, final String host) {
        return rest().given()
                .contentType(ContentType.JSON)
                .accept("application/json")
                .relaxedHTTPSValidation()
                .body(requestBody)
                .log()
                .all()
                .when()
                .put(host + contactUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    public Response deleteContact(final String systemName, final String id, final String host) {
        return rest().given()
                .accept(ContentType.JSON)
                .queryParam("SystemName", systemName)
                .queryParam("Id", id)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .delete(host + contactUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    @Override
    public Response deleteContact(final String ucid, final String host) {

        return rest().given()
                .accept(ContentType.JSON)
                .pathParam("ucid", ucid)
                .relaxedHTTPSValidation()
                .log()
                .all()
                .when()
                .get(host + contactUri)
                .then()
                .log()
                .all()
                .extract()
                .response();
    }
}
