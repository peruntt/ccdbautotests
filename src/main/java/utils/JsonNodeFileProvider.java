package utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonNodeFileProvider {

    public static JsonNode LoadJsonFromFile(final String filePath) {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode file = null;
        try {
            file = objectMapper.readTree(FileHelper.loadFile(filePath));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return file;
    }
}
