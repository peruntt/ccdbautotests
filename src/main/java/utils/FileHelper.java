package utils;

import java.io.File;

public class FileHelper {
    public static File loadFile(String path){

        File file = null;
        try {
            file = new File(path);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return file;
    }
}
