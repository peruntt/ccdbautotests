package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public  class PropertyHelper {
    private static Properties prop = new Properties();

    private static InputStream input = null;

    /**
     * @param path String value with path to the property file
     */
    public static Properties loadProperty(String path) {
        try {
            input = new FileInputStream(path);
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop;
    }

    public String getProperty(String name) {
        return prop.getProperty(name);
    }
}
