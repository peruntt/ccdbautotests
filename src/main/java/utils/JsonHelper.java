package utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonHelper {
    public static void updatePropertyValueInRequest(JsonNode request, String fieldName, String newValue) {

        try {
            JsonNode node = request.findParent(fieldName);
            ((ObjectNode) node).put(fieldName, newValue);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}