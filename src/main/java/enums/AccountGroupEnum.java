package enums;

public enum AccountGroupEnum {
    ITC("ITC"),
    NATIONAL_FLEET("NATIONAL FLEET"),
    TOP_NATIONAL_FLEET("TOP NATIONAL FLEET"),
    NORMAL_BUSINESS("NORMAL_BUSINESS");

    private final String accountGroup;

    private AccountGroupEnum(String  accountGroup) {
        this.accountGroup = accountGroup;
    }
    public String getValue() {
        return accountGroup;
    }
}
