package enums;

public enum CcdbSysNameEnum {

    UCID("UCID"),
    WINGS("WINGS"),
    KAMCOCKPIT("KAMCOCKPIT"),
    FLEETBOARD("FLEETBOARD");

    private final String sysName;

    private CcdbSysNameEnum(String sysName) {
        this.sysName = sysName;
    }

    public String getValue() {
        return sysName;
    }
}
