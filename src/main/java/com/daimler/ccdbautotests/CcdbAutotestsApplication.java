package com.daimler.ccdbautotests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.daimler")
public class CcdbAutotestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcdbAutotestsApplication.class, args);
    }

}