package com.daimler.ccdbautotests.test_data;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import services.CcdbAccountClientIf;
import services.TestDataProviderIf;
import utils.JsonHelper;
import utils.JsonNodeFileProvider;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dragan.perusinovic.bp@nttdata.ro
 * @date 10/12/2019
 */
public class GetAccountTestDataProviderImpl implements TestDataProviderIf {

    @Autowired
    private CcdbAccountClientIf ccdbAccountClient;
    private final HashMap<String, String> externalSysIds = new HashMap<String, String>();
    private final HashMap<String, JsonNode> testDataRequests = new HashMap<String, JsonNode>();
    private final HashMap<String, String> accountNames = new HashMap<String, String>();
    private final String filePath = "src/test/resources/createAccFromExternalSysId";

    @Override
    public void prepareTestData() {}

    @Override
    public void insertTestData(final String apiHost) {}

    @Step
    @Override
    public void prepareTestData(final String externalSysName) {
        final String integrationId = String.valueOf(new Random().nextInt(10000) + 50000);
        final String accountName = UUID.randomUUID().toString();
        externalSysIds.put(externalSysName, integrationId);
        accountNames.put(externalSysName,accountName);
        testDataRequests.put(externalSysName, loadAndPrepareRequest(filePath, accountName, externalSysName, integrationId));
    }

    @Step
    @Override
    public Response insertTestData(final String externalSysName, final String apiHostUrl) {
            JsonNode request = testDataRequests.get(externalSysName);
            Response response = ccdbAccountClient.createOrUpdateAccount(request, apiHostUrl);
            assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
            assertThat(response.getBody().asString()).isNotNull().isNotEmpty();
            return response;

    }

    @Step
    public String retrieveSysId(final String externalSysName){
        return externalSysIds.get(externalSysName);
    }

    @Step
    public String retrieveAccountName(final String externalSysName){
        return accountNames.get(externalSysName);
    }

    @Step
    public String retrieveUcid(final Response response, final String externalSysId) {
        JsonPath jsonResponseFleetBoard = response.getBody().jsonPath();
        List<Map<String, String>> externalSystems1 = jsonResponseFleetBoard.getList("externalSystemIds");
        String ucid;
        return ucid = externalSystems1.stream()
                .filter(item -> item.get("sourceSystemIdName").equals("UCID"))
                .map(el -> el.get("integrationId"))
                .collect(Collectors.toList())
                .stream()
                .filter(e -> !externalSysId.matches(e))
                .findAny()
                .orElse(null);
    }

    @Override
    public void removeTestData() {}

    private JsonNode loadAndPrepareRequest(final String filePath, final String accountName, final String sourceSystemName, final String sourceSystemId) {
        JsonNode request = JsonNodeFileProvider.LoadJsonFromFile(filePath);
        JsonHelper.updatePropertyValueInRequest(request, "accountName", accountName);
        JsonHelper.updatePropertyValueInRequest(request, "sourceSystemIdName", sourceSystemName);
        JsonHelper.updatePropertyValueInRequest(request, "integrationId", sourceSystemId);
        return request;
    }

}
