package com.daimler.ccdbautotests;

import net.serenitybdd.junit.spring.integration.SpringIntegrationSerenityRunner;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

@RunWith(SpringIntegrationSerenityRunner.class)
public abstract class SystemBaseTest {

    @Rule
    public final JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Before
    public void configureBaseUrl() {
    }

}
