package com.daimler.ccdbautotests.generic.v1.account.ucid;

import com.daimler.ccdbautotests.SystemBaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import services.CcdbAccountClientIf;
import services.CcdbAccountClientIfImpl;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = CcdbAccountClientIfImpl.class)
public class GetAccountByUcidTests_SunnyDay extends SystemBaseTest {

    @Steps
    @Autowired
    private CcdbAccountClientIf ccdbAccountClient;
    private EnvironmentVariables environmentVariables;
    private String apiHostUrl;

    @Override
    public void configureBaseUrl() {
        apiHostUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty("baseurl").orElse("https://tsac-int.dhc.corpintra.net");
    }

    @Ignore
    @Test
    public void getAccountByUcid_WhenUcidIsValid_ShouldFetchAccount() {

        final String ucId = "CH82A8CC63B40";

        Response ccdbResponse = ccdbAccountClient.getAccount(ucId, apiHostUrl);

        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(ccdbResponse.getBody().asString()).isNotNull();

        final JsonPath jsonResponse = ccdbResponse.getBody().jsonPath();
        softly.assertThat(jsonResponse.getString("ucid")).isEqualTo(ucId);

    }

    @Test
    public void getAccountByUcid_WhenUcidHasNoDBRecord_ShouldReturnResponse204() {

        final String ucId = "00000000000";

        Response ccdbResponse = ccdbAccountClient.getAccount(ucId, apiHostUrl);

        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.NO_CONTENT.value());
        assertThat(ccdbResponse.getBody().asString()).isNullOrEmpty();
        assertThat(ccdbResponse.getHeader(HttpHeaders.ETAG)).isEqualTo("\"No entity found for UCID.\"");
    }

}
