package com.daimler.ccdbautotests.generic.v1.account.get;

import com.daimler.ccdbautotests.SystemBaseTest;
import enums.CcdbSysNameEnum;
import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import services.CcdbAccountClientIf;
import services.CcdbAccountClientIfImpl;

import static org.assertj.core.api.Assertions.assertThat;


@ContextConfiguration(classes = CcdbAccountClientIfImpl.class)
public class GetAccountTests_RainyDay extends SystemBaseTest {

    @Steps
    @Autowired
    private CcdbAccountClientIf ccdbAccountClient;
    private EnvironmentVariables environmentVariables;
    private String apiHostUrl;


    @Before
    public void setUp() {

        apiHostUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty("baseurl").orElse("https://tsac-int.dhc.corpintra.net");
    }

    @Test
    public void getAccount_WhenWithoutQueryParams_ShouldReturnErrorResponse422() {

        Response ccdbResponse = ccdbAccountClient.getAccount(null, null, apiHostUrl);

        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
        assertThat(ccdbResponse.getBody().asString()).isNullOrEmpty();
        assertThat(ccdbResponse.getHeader(HttpHeaders.ETAG)).isEqualTo("\"Input data invalid - Mandatory id argument is missing.\"");
    }

    @Test
    public void getAccount_WhenWithSysNameAndNoId_ShouldReturnErrorResponse422() {

        final String systemName = CcdbSysNameEnum.WINGS.getValue();

        Response ccdbResponse = ccdbAccountClient.getAccount(systemName, null, apiHostUrl);

        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
        assertThat(ccdbResponse.getBody().asString()).isNullOrEmpty();
        assertThat(ccdbResponse.getHeader(HttpHeaders.ETAG)).isEqualTo("\"Input data invalid - Mandatory id argument is missing.\"");
    }

}
