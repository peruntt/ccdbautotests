package com.daimler.ccdbautotests.generic.v1.account.get;

import com.daimler.ccdbautotests.SystemBaseTest;
import com.daimler.ccdbautotests.test_data.GetAccountTestDataProviderImpl;
import enums.CcdbSysNameEnum;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import services.CcdbAccountClientIf;
import services.CcdbAccountClientIfImpl;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {CcdbAccountClientIfImpl.class, GetAccountTestDataProviderImpl.class})
public class GetAccountTests_SunnyDay extends SystemBaseTest {

    @Steps
    @Autowired
    private CcdbAccountClientIf ccdbAccountClient;

    @Autowired
    private  GetAccountTestDataProviderImpl testDataProvider;

    private EnvironmentVariables environmentVariables;

    private String apiHostUrl;

    @Override
    public void configureBaseUrl() {

        apiHostUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty("baseurl").orElse("https://tsac-int.dhc.corpintra.net");
    }

    @Test
    public void getAccount_WhenSystemWingsAndValidId_ShouldFetchAccount() {

        //test data preparation
        final String systemName = CcdbSysNameEnum.WINGS.getValue();
        testDataProvider.prepareTestData(systemName);
        final Response testDataResponse = testDataProvider.insertTestData(systemName,apiHostUrl);
        final String systemId = testDataProvider.retrieveSysId(systemName);
        final String ucId = testDataProvider.retrieveUcid(testDataResponse, systemName);
        final String accountName = testDataProvider.retrieveAccountName(systemName);

        //action
        Response ccdbResponse = ccdbAccountClient.getAccount(systemName, systemId, apiHostUrl);

        //assertions
        assertThat(ccdbResponse).isNotNull();
        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.OK.value());

        JsonPath jsonResponse = ccdbResponse.getBody().jsonPath();
        softly.assertThat(jsonResponse.getString("accountName")).isEqualToIgnoringCase(accountName);

        List<Map<String, String>> externalSystems = jsonResponse.getList("externalSystemIds");
        softly.assertThat(externalSystems).isNotNull().isNotEmpty().size().isGreaterThan(1);
        for (Map<String, String> externalSys : externalSystems) {

            softly.assertThat(externalSys).size().isGreaterThan(1)
                    .isLessThanOrEqualTo(4)
                    .returnToMap()
                    .containsKeys("integrationId", "sourceSystemIdName")
                    .extracting("integrationId", "sourceSystemIdName")
                    .containsAnyOf(systemId, systemName, ucId, "UCID");
        }
    }

    @Test
    public void getAccount_WhenSystemKamcockpitAndValidId_ShouldFetchAccount() {

        //test data preparation
        final String systemName = CcdbSysNameEnum.KAMCOCKPIT.getValue();
        testDataProvider.prepareTestData(systemName);
        final Response testDataResponse = testDataProvider.insertTestData(systemName,apiHostUrl);
        final String systemId = testDataProvider.retrieveSysId(systemName);
        final String ucId = testDataProvider.retrieveUcid(testDataResponse, systemName);
        final String accountName = testDataProvider.retrieveAccountName(systemName);

        //action
        Response ccdbResponse = ccdbAccountClient.getAccount(systemName, systemId, apiHostUrl);

        //assertions
        assertThat(ccdbResponse).isNotNull();
        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.OK.value());

        JsonPath jsonResponse = ccdbResponse.getBody().jsonPath();
        softly.assertThat(jsonResponse.getString("accountName")).isEqualToIgnoringCase(accountName);

        List<Map<String, String>> externalSystems = jsonResponse.getList("externalSystemIds");
        softly.assertThat(externalSystems).isNotNull().isNotEmpty().size().isGreaterThan(1);
        for (Map<String, String> externalSys : externalSystems) {

            softly.assertThat(externalSys).size().isGreaterThan(1)
                    .isLessThanOrEqualTo(4)
                    .returnToMap()
                    .containsKeys("integrationId", "sourceSystemIdName")
                    .extracting("integrationId", "sourceSystemIdName")
                    .containsAnyOf(systemId, systemName, ucId, "UCID");
        }
    }

    @Ignore
    @Test
    public void getAccount_WhenValidUcidAndNoSystem_ShouldFetchAccount() {

        //test data preparation
        final String systemName = CcdbSysNameEnum.UCID.getValue();
        testDataProvider.prepareTestData(systemName);
        testDataProvider.insertTestData(systemName,apiHostUrl);
        final String ucId = testDataProvider.retrieveSysId(systemName);
        final String accountName = testDataProvider.retrieveAccountName(systemName);

        Response ccdbResponse = ccdbAccountClient.getAccount(null, ucId, apiHostUrl);

        assertThat(ccdbResponse).isNotNull();
        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.OK.value());

        JsonPath jsonResponse = ccdbResponse.getBody().jsonPath();
        softly.assertThat(jsonResponse.getString("externalSystemIds.sourceSystemIdName[0]")).isEqualToIgnoringCase("UCID");
        softly.assertThat(jsonResponse.getString("externalSystemIds.integrationId[0]")).isEqualToIgnoringCase(ucId);
        softly.assertThat(jsonResponse.getString("accountName")).isEqualToIgnoringCase(accountName);
    }

    @Test
    public void getAccount_WhenWithUcidButThereIsNoDbRecord_ShouldReturnResponse204() {

        final String ucId = "00000000000";
        final String systemName = CcdbSysNameEnum.WINGS.getValue();

        Response ccdbResponse = ccdbAccountClient.getAccount(systemName, ucId, apiHostUrl);

        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.NO_CONTENT.value());
        assertThat(ccdbResponse.getBody().asString()).isNullOrEmpty();
        assertThat(ccdbResponse.getHeader(HttpHeaders.ETAG)).isEqualTo("\"No entity found for ID.\"");
    }

    @Test
    public void getAccount_WhenWithSysNameAndIdButThereIsNoDbRecord_ShouldReturnResponse204() {

        final String systemName = CcdbSysNameEnum.WINGS.getValue();
        final String systemId = "00000000000";

        Response ccdbResponse = ccdbAccountClient.getAccount(systemName, systemId, apiHostUrl);

        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.NO_CONTENT.value());
        assertThat(ccdbResponse.getBody().asString()).isNullOrEmpty();
        assertThat(ccdbResponse.getHeader(HttpHeaders.ETAG)).isEqualTo("\"No entity found for ID.\"");
    }

}


