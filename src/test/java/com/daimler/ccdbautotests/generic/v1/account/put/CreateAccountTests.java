package com.daimler.ccdbautotests.generic.v1.account.put;

import com.daimler.ccdbautotests.SystemBaseTest;
import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import services.CcdbAccountClientIf;
import services.CcdbAccountClientIfImpl;
import utils.JsonHelper;
import utils.JsonNodeFileProvider;

import java.io.IOException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = CcdbAccountClientIfImpl.class)
public class CreateAccountTests extends SystemBaseTest {

    @Steps
    @Autowired
    private CcdbAccountClientIf ccdbAccountClient;
    private EnvironmentVariables environmentVariables;
    private String apiHostUrl;

    @Before
    public void setUp() {

        apiHostUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty("baseurl").orElse("https://tsac-int.dhc.corpintra.net");
    }

    @Test
    public void createAccount_WhenBasicDataIsProvided_ShouldCreateAccount() throws IOException {

        JsonNode createRequest = JsonNodeFileProvider.LoadJsonFromFile("src/test/resources/createAccountRequest.json");

        final String accountName = UUID.randomUUID().toString();
        JsonHelper.updatePropertyValueInRequest(createRequest, "accountName", accountName);

        Response ccdbResponse = ccdbAccountClient.createOrUpdateAccount(createRequest, apiHostUrl);

        assertThat(ccdbResponse).isNotNull();
        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.OK.value());

        JsonPath jsonResponse = ccdbResponse.getBody().jsonPath();

        softly.assertThat(jsonResponse.getString("ucid")).isNotNull().isNotEmpty();
        softly.assertThat(jsonResponse.getString("accountName")).isEqualToIgnoringCase(accountName);
        softly.assertThat(jsonResponse.getString("status")).isEqualToIgnoringCase("Active");
    }

    @Test
    public void createAccount_WhenVatDataIsProvided_ShouldCreateAccount(){

        JsonNode createRequest = JsonNodeFileProvider.LoadJsonFromFile("src/test/resources/createAccountBasicRequestWithVat");
        final String accountName = UUID.randomUUID().toString();
        JsonHelper.updatePropertyValueInRequest(createRequest, "accountName", accountName);

        Response ccdbResponse = ccdbAccountClient.createOrUpdateAccount(createRequest, apiHostUrl);

        assertThat(ccdbResponse).isNotNull();
        assertThat(ccdbResponse.statusCode()).isEqualTo(HttpStatus.OK.value());

        JsonPath jsonResponse = ccdbResponse.getBody().jsonPath();

        softly.assertThat(jsonResponse.getString("ucid")).isNotNull().isNotEmpty();
        softly.assertThat(jsonResponse.getString("accountName")).isEqualToIgnoringCase(accountName);
        softly.assertThat(jsonResponse.getString("status")).isEqualToIgnoringCase("Active");
    }

}
